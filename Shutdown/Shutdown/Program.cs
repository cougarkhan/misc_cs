﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Shell32;

namespace Shutdown
{
    class Program
    {
        [STAThread]
        static void Main(string[] args)
        {
            Shell shell = new Shell();
            shell.ShutdownWindows();
        }
    }
}
