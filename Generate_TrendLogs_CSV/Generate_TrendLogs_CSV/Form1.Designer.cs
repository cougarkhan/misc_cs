﻿namespace Generate_TrendLogs_CSV
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.messages = new System.Windows.Forms.ListBox();
            this.button_csv = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // messages
            // 
            this.messages.FormattingEnabled = true;
            this.messages.ItemHeight = 16;
            this.messages.Location = new System.Drawing.Point(28, 24);
            this.messages.Name = "messages";
            this.messages.Size = new System.Drawing.Size(480, 404);
            this.messages.TabIndex = 0;
            // 
            // button_csv
            // 
            this.button_csv.Location = new System.Drawing.Point(178, 495);
            this.button_csv.Name = "button_csv";
            this.button_csv.Size = new System.Drawing.Size(119, 23);
            this.button_csv.TabIndex = 1;
            this.button_csv.Text = "Generate CSV";
            this.button_csv.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(542, 610);
            this.Controls.Add(this.button_csv);
            this.Controls.Add(this.messages);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListBox messages;
        private System.Windows.Forms.Button button_csv;
    }
}

