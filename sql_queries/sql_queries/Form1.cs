﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace sql_queries
{
    public partial class Form1 : Form
    {
        List<string> listbox_values = new List<string>();

        public Form1()
        {
            InitializeComponent();
            string settings_file = @".\etc\settings.json";
            Settings config = new Settings(settings_file);
            Mssql mssql = config.server.mssql;
            bool result = new Boolean();

            ProgramFunctions pf = new ProgramFunctions(listBox1, listBox2, listbox_values, config.server.debug.flag);

            SqlConnection mssql_conn = pf.GetMSSqlConnection(mssql);
            if (mssql_conn == null)
            {
                pf.update_list(listBox1,listbox_values,"ERROR: MSSQL Connection Object could not be created.");
                //Environment.Exit(2);
            }
            SqlCommand mssql_cmd = pf.GetMSSqlCommand(mssql_conn);
            if (mssql_cmd == null)
            {
                pf.update_list(listBox1, listbox_values, "ERROR: MSSQL Command Object could not be created.");
                //Environment.Exit(2);
            }
            mssql_conn = pf.OpenMSSqlConnection(mssql_conn);
            if (mssql_conn == null)
            {
                pf.update_list(listBox1, listbox_values, "ERROR: MSSQL Connection could not be opened.");
                //Environment.Exit(2);
            }
            if (mssql_conn != null && mssql_conn.State == ConnectionState.Open)
            {
                result = pf.ExecuteMSSqlQuery(mssql_cmd, mssql);
                mssql_conn = pf.CloseMSSqlConnection(mssql_conn);
                if (result)
                {
                    pf.update_list(listBox1, listbox_values, "MSSQL SQL Query Succeeded.");
                    //Environment.Exit(0);
                }
                else
                {
                    pf.update_list(listBox1, listbox_values, "ERROR: MSSQL SQL Query Failed.");
                    //Environment.Exit(2);
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void listBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            var value = listBox2.SelectedValue;
            textBox1.Text = value.ToString();
        }

        
    }
}
