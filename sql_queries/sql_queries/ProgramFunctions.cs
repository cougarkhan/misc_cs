﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using System.Net.Mail;
using System.Security.Cryptography;
using System.IO;
using System.Windows.Forms;

namespace sql_queries
{
    class ProgramFunctions
    {
        bool DEBUG = false;
        LogFunctions lf = new LogFunctions();
        List<string> list1 = new List<string>();
        ListBox listbox1 = new ListBox();
        ListBox listbox2 = new ListBox();

        public ProgramFunctions(ListBox lb1, ListBox lb2, List<string> ls1, bool debug_flag)
        {
            listbox1 = lb1;
            listbox2 = lb2;
            list1 = ls1;
            DEBUG = debug_flag;
        }

        public SqlConnection GetMSSqlConnection(Mssql server)
        {
            string conn_string = "";

            if (server.trusted_conn == false)
            {
                string password = Decrypt(server.password, server.password_phrase);
                conn_string = "server=" + server.instance +
                                ";User ID=" + server.username + ";" +
                                ";Password=" + password + ";" +
                                ";Trusted_Connection=no;" +
                                "database=" + server.database +
                                ";connection timeout=5";
            }
            else
            {
                conn_string = "server=" + server.instance +
                                    ";Trusted_Connection=yes;" +
                                    "database=" + server.database +
                                    ";connection timeout=5";
            }
            try
            {
                if (DEBUG) { update_list(listbox1,list1,"Creating MSSQL Connection Object."); }
                SqlConnection conn = new SqlConnection(conn_string);
                if (DEBUG) { update_list(listbox1,list1,"Created MSSQL Connection Object Successfully."); }
                return conn;
            }
            catch (Exception e)
            {
                if (DEBUG) { update_list(listbox1,list1,"Create MSSQL Connection Object Failed."); }
                return null;
            }
        }

        public SqlCommand GetMSSqlCommand(SqlConnection conn)
        {
            try
            {
                if (DEBUG) { update_list(listbox1,list1,"Creating MSSQL Command Object."); }
                SqlCommand cmd = conn.CreateCommand();
                if (DEBUG) { update_list(listbox1,list1,"Created MSSQL Command Object Successfully."); }
                return cmd;
            }
            catch (Exception e)
            {
                if (DEBUG) { update_list(listbox1, list1, "Create MSSQL Command Object Failed."); }
                return null;
            }
        }

        public SqlConnection OpenMSSqlConnection(SqlConnection conn)
        {
            try
            {
                if (DEBUG) { update_list(listbox1,list1,"Opening MSSQL Connection Object."); }
                conn.Open();
                if (conn.State == ConnectionState.Open)
                {
                    if (DEBUG) { update_list(listbox1, list1, "MSSQL Connection Object State is " + conn.State + "."); }
                    if (DEBUG) { update_list(listbox1,list1,"Opened MSSQL Connection Object Successfully."); }
                    return conn;
                }
                else
                {
                    if (DEBUG) { update_list(listbox1, list1, "MSSQL Connection Object State is " + conn.State + "."); }
                    return null;
                }
            }
            catch (Exception e)
            {
                if (DEBUG) { update_list(listbox1, list1, "Open MSSQL Connection Object Failed."); }
                return null;
            }

        }

        public SqlConnection CloseMSSqlConnection(SqlConnection conn)
        {
            try
            {
                if (DEBUG) { update_list(listbox1,list1,"Closing MSSQL Connection Object."); }
                conn.Dispose();
                if (conn.State == ConnectionState.Closed)
                {
                    if (DEBUG) { update_list(listbox1, list1, "MSSQL Connection Object State is " + conn.State + "."); }
                    if (DEBUG) { update_list(listbox1,list1,"Closed MSSQL Connection Object Successfully."); }
                    return conn;
                }
                else
                {
                    if (DEBUG) { update_list(listbox1,list1,"MSSQL Connection Object State is " + conn.State + "."); }
                    return null;
                }
            }
            catch (Exception e)
            {
                if (DEBUG) { update_list(listbox1, list1, "Close MSSQL Connection Object Failed."); }
                return null;
            }
        }

        public bool ExecuteMSSqlQuery(SqlCommand cmd, Mssql server)
        {
            try
            {
                cmd.CommandText = server.sql_query;
                if (DEBUG) { update_list(listbox1,list1,"Executing SQL Query."); }
                SqlDataReader reader = cmd.ExecuteReader();

                List<string> rows = new List<string>();
                while (reader.Read())
                {
                    rows.Add(reader[0].ToString());
                }

                if (rows.Count > 0)
                {
                    if (DEBUG) { update_list(listbox1,list1,"Executed SQL Query Successfully."); }
                    update_list(listbox2, rows);
                    return true;
                }
                else
                {
                    if (DEBUG) { update_list(listbox1,list1,"Execute SQL Query Failed."); }
                    return false;
                }
            }
            catch (Exception e)
            {
                if (DEBUG) { update_list(listbox1, list1, "Execute SQL Query Failed."); }
                return false;
            }
        }

        public byte[] AES_Encrypt(byte[] bytesToBeEncrypted, byte[] passwordBytes)
        {
            byte[] encryptedBytes = null;

            // Set your salt here, change it to meet your flavor:
            // The salt bytes must be at least 8 bytes.
            byte[] saltBytes = new byte[] { 1, 2, 3, 4, 5, 6, 7, 8 };

            using (MemoryStream ms = new MemoryStream())
            {
                using (RijndaelManaged AES = new RijndaelManaged())
                {
                    AES.KeySize = 256;
                    AES.BlockSize = 128;

                    var key = new Rfc2898DeriveBytes(passwordBytes, saltBytes, 1000);
                    AES.Key = key.GetBytes(AES.KeySize / 8);
                    AES.IV = key.GetBytes(AES.BlockSize / 8);

                    AES.Mode = CipherMode.CBC;

                    using (var cs = new CryptoStream(ms, AES.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(bytesToBeEncrypted, 0, bytesToBeEncrypted.Length);
                        cs.Close();
                    }
                    encryptedBytes = ms.ToArray();
                }
            }

            return encryptedBytes;
        }

        public byte[] AES_Decrypt(byte[] bytesToBeDecrypted, byte[] passwordBytes)
        {
            byte[] decryptedBytes = null;

            // Set your salt here, change it to meet your flavor:
            // The salt bytes must be at least 8 bytes.
            byte[] saltBytes = new byte[] { 1, 2, 3, 4, 5, 6, 7, 8 };

            using (MemoryStream ms = new MemoryStream())
            {
                using (RijndaelManaged AES = new RijndaelManaged())
                {
                    AES.KeySize = 256;
                    AES.BlockSize = 128;

                    var key = new Rfc2898DeriveBytes(passwordBytes, saltBytes, 1000);
                    AES.Key = key.GetBytes(AES.KeySize / 8);
                    AES.IV = key.GetBytes(AES.BlockSize / 8);

                    AES.Mode = CipherMode.CBC;

                    using (var cs = new CryptoStream(ms, AES.CreateDecryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(bytesToBeDecrypted, 0, bytesToBeDecrypted.Length);
                        cs.Close();
                    }
                    decryptedBytes = ms.ToArray();
                }
            }

            return decryptedBytes;
        }

        public byte[] GetRandomBytes()
        {
            int _saltSize = 4;
            byte[] ba = new byte[_saltSize];
            RNGCryptoServiceProvider.Create().GetBytes(ba);
            return ba;
        }

        public string Encrypt(string text, string pwd)
        {
            byte[] originalBytes = Encoding.UTF8.GetBytes(text);
            byte[] encryptedBytes = null;
            byte[] passwordBytes = Encoding.UTF8.GetBytes(pwd);

            // Hash the password with SHA256
            passwordBytes = SHA256.Create().ComputeHash(passwordBytes);

            // Generating salt bytes
            byte[] saltBytes = GetRandomBytes();

            // Appending salt bytes to original bytes
            byte[] bytesToBeEncrypted = new byte[saltBytes.Length + originalBytes.Length];
            for (int i = 0; i < saltBytes.Length; i++)
            {
                bytesToBeEncrypted[i] = saltBytes[i];
            }
            for (int i = 0; i < originalBytes.Length; i++)
            {
                bytesToBeEncrypted[i + saltBytes.Length] = originalBytes[i];
            }

            encryptedBytes = AES_Encrypt(bytesToBeEncrypted, passwordBytes);

            return Convert.ToBase64String(encryptedBytes);
        }

        public string Decrypt(string decryptedText, string pwd)
        {
            byte[] bytesToBeDecrypted = Convert.FromBase64String(decryptedText);
            byte[] passwordBytes = Encoding.UTF8.GetBytes(pwd);

            // Hash the password with SHA256
            passwordBytes = SHA256.Create().ComputeHash(passwordBytes);

            byte[] decryptedBytes = AES_Decrypt(bytesToBeDecrypted, passwordBytes);

            // Getting the size of salt
            int _saltSize = 4;

            // Removing salt bytes, retrieving original bytes
            byte[] originalBytes = new byte[decryptedBytes.Length - _saltSize];
            for (int i = _saltSize; i < decryptedBytes.Length; i++)
            {
                originalBytes[i - _saltSize] = decryptedBytes[i];
            }

            return Encoding.UTF8.GetString(originalBytes);
        }

        public void update_list(ListBox lb, List<string> list, string value)
        {
            list.Add(value);
            lb.DataSource = null;
            lb.DataSource = list;
        }

        public void update_list(ListBox lb, List<string> list)
        {
            lb.DataSource = null;
            lb.DataSource = list;
        }
    }
}
