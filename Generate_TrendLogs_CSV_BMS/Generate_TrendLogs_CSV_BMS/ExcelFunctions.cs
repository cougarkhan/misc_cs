﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Office.Interop.Excel;
using System.Data;
using System.Data.SqlClient;

namespace Generate_TrendLogs_CSV_BMS
{
    class ExcelFunctions
    {
        private Application excel;
        private Workbook wb;
        public Worksheet ws;
        public ExcelFunctions()
        {
            excel = new Application();
            excel.Visible = false;
            excel.DisplayAlerts = false;

            wb = excel.Workbooks.Add(Type.Missing);
        }

        //public void addWorksheet(System.Data.DataTable dt, string sheetName)
        //{
        //    ws = wb.Worksheets.Add(Type.Missing);
        //    ws.Name = sheetName;
        //    int rowCount = dt.Rows.Count;
        //    int colCount = dt.Columns.Count;
        //    int colCounter = 1;
        //    int rowCounter = 2;
            
        //    foreach (System.Data.DataColumn col in dt.Columns)
        //    {
        //        if (colCounter <= colCount)
        //        {
        //            //Console.WriteLine("wrote {0}", col.ColumnName);
        //            ws.Cells[1, colCounter] = col.ColumnName;
        //        }

        //        colCounter++;
        //    }

        //    foreach (System.Data.DataRow row in dt.Rows)
        //    {
        //        if (rowCounter <= rowCount)
        //        {
        //            colCounter = 1;
        //            foreach (System.Data.DataColumn col in dt.Columns)
        //            {
        //                //Console.WriteLine("wrote {0}", row[col]);
        //                ws.Cells[rowCounter, colCounter] = row[col];
        //                colCounter++;
        //            }
        //        }
        //        rowCounter++;
        //    }
        //}

        public void addWorksheet(System.Data.DataTable dt, string sheetName)
        {
            ws = wb.Worksheets.Add(Type.Missing);
            ws.Name = sheetName;
            int rowCount = dt.Rows.Count;
            int colCount = dt.Columns.Count;
            int topRow = 2;
            object[,] arr = new object[dt.Rows.Count, dt.Columns.Count];

            int colCounter = 1;

            foreach (System.Data.DataColumn col in dt.Columns)
            {
                if (colCounter <= colCount)
                {
                    //Console.WriteLine("wrote {0}", col.ColumnName);
                    ws.Cells[1, colCounter] = col.ColumnName;
                }

                colCounter++;
            }

            for (int r = 0; r < dt.Rows.Count; r++)
            {
                DataRow dr = dt.Rows[r];
                for (int c = 0; c < dt.Columns.Count; c++)
                {
                    arr[r, c] = dr[c];
                }
            }

            Range c1 = (Range)ws.Cells[topRow, 1];
            Range c2 = (Range)ws.Cells[topRow + dt.Rows.Count - 1, dt.Columns.Count];
            Range range = ws.get_Range(c1, c2);

            range.Value = arr;
        }

        public void saveWorkbookCSV(string path)
        {
            Console.WriteLine("Saved book");
            wb.SaveAs(path, Microsoft.Office.Interop.Excel.XlFileFormat.xlCSV);
            wb.SaveAs(path);
            Console.WriteLine("Closed book");
            wb.Close();
        }

        public void saveWorkbookXLSX(string path)
        {
            Console.WriteLine("Saved book");
            wb.SaveAs(path);
            Console.WriteLine("Closed book");
            wb.Close();
        }

        public void exitExcel()
        {
            Console.WriteLine("Quit Excel");
            excel.Quit();
        }
    }
}
