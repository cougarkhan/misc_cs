﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.IO;

namespace Generate_TrendLogs_CSV_BMS
{
    class Program
    {
        static void Main(string[] args)
        {
            bool DEBUG = false;
            string query1 = "SELECT TLInstance,Name,MonObjObjectName FROM TL";
            string query2;
            string connString = "server=bis-sqlhist-prd.ead.ubc.ca;Trusted_Connection=yes;database=HistorianDB;connection timeout=5";
            DataTable dt1;
            DataTable dt2;
            DataFunctions df = new DataFunctions();
            ExcelFunctions ef = new ExcelFunctions();

            if (DEBUG) { Console.WriteLine("Pulling TL Names"); }
            dt1 = df.PullData(query1, connString);

            foreach (DataRow row in dt1.Rows)
            {
                //foreach (DataColumn col in dt1.Columns)
                //{
                //    Console.Write(row[col] + " | ");
                //}
                //Console.Write("{0} {1}", row["TLInstance"], row["Name"]);
                //Console.WriteLine("");
                dt2 = new DataTable();
                query2 = "SELECT * FROM TLData WHERE TLInstance = " + row["TLInstance"] + " ORDER BY RecordNumber ASC";
                if (DEBUG) { Console.WriteLine("Pulling TL Data {0}", row["TLInstance"]); }
                dt2 = df.PullData(query2, connString);

                string newSheetName = row["Name"].ToString();
                newSheetName = newSheetName.Substring(0,25);
                newSheetName = newSheetName + row["TLInstance"].ToString();
                if (DEBUG) {Console.WriteLine("Adding Sheet {0}", newSheetName); }
                ef.addWorksheet(dt2, newSheetName);


                var result = new StringBuilder();

                for (int i = 0; i < dt2.Columns.Count; i++)
                {
                    result.Append(dt2.Columns[i].ColumnName);
                    result.Append(i == dt2.Columns.Count - 1 ? "\n" : ",");
                }

                foreach (DataRow row2 in dt2.Rows)
                {
                    for (int i = 0; i < dt2.Columns.Count; i++)
                    {
                        result.Append(row2[i].ToString());
                        result.Append(i == dt2.Columns.Count - 1 ? "\n" : ",");
                    }
                }

                File.WriteAllText(@"C:\temp\sust_test\" + row["Name"] + ".csv", result.ToString());
                
            }
            //ef.saveWorkbookCSV(@"C:\temp\sust_test\Ponderosa_TrendLogs.csv");
            ef.saveWorkbookXLSX(@"C:\temp\sust_test\Ponderosa_TrendLogs.xlsx");
            ef.exitExcel();
            //Console.ReadKey();
        }
    }
}
