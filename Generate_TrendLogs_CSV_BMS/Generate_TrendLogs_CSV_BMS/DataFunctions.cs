﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace Generate_TrendLogs_CSV_BMS
{
    class DataFunctions
    {

        public DataFunctions()
        {

        }

        public DataTable PullData(string query, string connString)
        {
            bool DEBUG = false;
            if (DEBUG) { Console.WriteLine("Pulling {0}", query); }
            DataTable dt = new DataTable();
            SqlConnection conn = new SqlConnection(connString);
            SqlCommand cmd = new SqlCommand(query, conn);
            conn.Open();

            SqlDataAdapter da1 = new SqlDataAdapter(cmd);

            da1.Fill(dt);
            conn.Close();
            da1.Dispose();
            if (DEBUG) { Console.WriteLine("Returning DT"); }
            return dt;
        }

    }
}
