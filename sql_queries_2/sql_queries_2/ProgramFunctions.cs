﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using System.IO;
using System.Windows.Forms;

namespace sql_queries_2
{
    class ProgramFunctions
    {
        public AppObjects root { get; set; }
        public DataFunctions df { get; set; }
        public ListBox lb1 { get; set; }
        public ListBox lb2 { get; set; }

        public ProgramFunctions()
        {
            root = new AppObjects(@".\etc\settings.json");
            root.ro.messages = new List<string>();
            root.ro.results = new List<string>();
            df = new DataFunctions();
        }

    }
}
