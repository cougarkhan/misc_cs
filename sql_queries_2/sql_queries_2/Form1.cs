﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace sql_queries_2
{
    public partial class Form1 : Form
    {
        ProgramFunctions pf;
        AppObjects root;

        public Form1()
        {
            InitializeComponent();
            pf = new ProgramFunctions();
            root = pf.root;
            Messages.DataSource = root.ro.messages; 
            Data.DataSource = root.ro.results;
            
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            load_tables();
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        public void update_listbox(ListBox lb, List<string> values)
        {
            lb.DataSource = null;
            lb.DataSource = values;
            lb.Refresh();
        }

        public void update_listbox()
        {
            Messages.Refresh();
            Data.Refresh();
        }

        public void load_tables()
        {
            bool result;

            root = pf.df.GetMSSqlConnection(root);
            root = pf.df.GetMSSqlCommand(root);
            root = pf.df.OpenMSSqlConnection(root);

            if (root.ro.conn != null && root.ro.conn.State == ConnectionState.Open)
            {
                result = pf.df.ExecuteMSSqlQuery(root);
                root = pf.df.CloseMSSqlConnection(root);

                if (result)
                {
                    update_listbox(Messages, root.ro.messages);
                    update_listbox(Data, root.ro.results);
                }
                else
                {
                    root.ro.messages.Add("MSSQL SQL Query Failed.");
                }
            }
        }

        public void dump_data_to_file()
        {
            bool result;

            
            root = pf.df.GetMSSqlConnection(root);
            root = pf.df.GetMSSqlCommand(root);
            root = pf.df.OpenMSSqlConnection(root);
            root.server.mssql.sql_query = "SELECT *

            if (root.ro.conn != null && root.ro.conn.State == ConnectionState.Open)
            {
                result = pf.df.ExecuteMSSqlQuery(root);
                root = pf.df.CloseMSSqlConnection(root);

                if (result)
                {
                    update_listbox(Messages, root.ro.messages);
                    update_listbox(Data, root.ro.results);
                }
                else
                {
                    root.ro.messages.Add("MSSQL SQL Query Failed.");
                }
            }
        }
    }
}
