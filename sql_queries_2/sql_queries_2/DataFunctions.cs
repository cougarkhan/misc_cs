﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;

namespace sql_queries_2
{
    class DataFunctions
    {
        
        public DataFunctions()
        {
        }

        public AppObjects GetMSSqlConnection(AppObjects root)
        {
            bool DEBUG = root.server.debug.flag;
            string conn_string = "";
            conn_string = "server=" + root.server.mssql.instance +
                                ";Trusted_Connection=yes;" +
                                "database=" + root.server.mssql.database +
                                ";connection timeout=5";
            SqlConnection conn = new SqlConnection(conn_string);

            try
            {
                root.ro.messages.Add("Connecting to " + root.server.mssql.instance + "...");
                if (DEBUG) { root.ro.messages.Add("Creating MSSQL Connection Object."); }
                if (DEBUG) { root.ro.messages.Add("Created MSSQL Connection Object Successfully."); }
                root.ro.conn = conn;
                return root;
            }
            catch (Exception e)
            {
                if (DEBUG) { root.ro.messages.Add("Create MSSQL Connection Object Failed."); }
                if (DEBUG) { root.ro.messages.Add("Connection String: " + conn_string); }
                if (DEBUG) { root.ro.messages.Add(e.Message); }
                return root;
            }
        }

        public AppObjects GetMSSqlCommand(AppObjects root)
        {
            bool DEBUG = root.server.debug.flag;
            SqlCommand cmd = root.ro.conn.CreateCommand();

            try
            {
                if (DEBUG) { root.ro.messages.Add("Creating MSSQL Command Object."); }
                if (DEBUG) { root.ro.messages.Add("Created MSSQL Command Object Successfully."); }
                root.ro.cmd = cmd;
                return root;
            }
            catch (Exception e)
            {
                if (DEBUG) { root.ro.messages.Add("Create MSSQL Command Object Failed."); }
                if (DEBUG) { root.ro.messages.Add(e.Message); }
                return root;
            }
        }

        public AppObjects OpenMSSqlConnection(AppObjects root)
        {
            bool DEBUG = root.server.debug.flag;
            SqlConnection conn = root.ro.conn;

            try
            {
                if (DEBUG) { root.ro.messages.Add("Opening MSSQL Connection Object."); }
                conn.Open();
                if (conn.State == ConnectionState.Open)
                {
                    if (DEBUG) { root.ro.messages.Add("MSSQL Connection Object State is " + conn.State + "."); }
                    root.ro.messages.Add("Opened MSSQL Connection Object Successfully.");
                    root.ro.conn = conn;
                    return root;
                }
                else
                {
                    if (DEBUG) { root.ro.messages.Add("MSSQL Connection Object State is " + conn.State + "."); }
                    return root;
                }
            }
            catch (Exception e)
            {
                root.ro.messages.Add("Open MSSQL Connection Object Failed.");
                if (DEBUG) { root.ro.messages.Add(e.Message); }
                return root;
            }

        }

        public AppObjects CloseMSSqlConnection(AppObjects root)
        {
            bool DEBUG = root.server.debug.flag;
            SqlConnection conn = root.ro.conn;

            try
            {
                root.ro.messages.Add("Disconnecting from " + root.server.mssql.instance + ".");
                if (DEBUG) { root.ro.messages.Add("Closing MSSQL Connection Object."); }
                conn.Dispose();
                if (conn.State == ConnectionState.Closed)
                {
                    if (DEBUG) { root.ro.messages.Add("MSSQL Connection Object State is " + conn.State + "."); }
                    root.ro.messages.Add("Closed MSSQL Connection Object Successfully.");
                    root.ro.conn = conn;
                    return root;
                }
                else
                {
                    if (DEBUG) { root.ro.messages.Add("MSSQL Connection Object State is " + conn.State + "."); }
                    return root;
                }
            }
            catch (Exception e)
            {
                root.ro.messages.Add("Close MSSQL Connection Object Failed.");
                if (DEBUG) { root.ro.messages.Add(e.Message); }
                return root;
            }
        }

        public bool ExecuteMSSqlQuery(AppObjects root)
        {
            bool DEBUG = root.server.debug.flag;
            SqlCommand cmd = root.ro.cmd;
            Mssql server = root.server.mssql;
            try
            {
                cmd.CommandText = server.sql_query;
                root.ro.messages.Add("Executing SQL Query.");
                SqlDataReader reader = cmd.ExecuteReader();

                List<string> rows = new List<string>();
                while (reader.Read())
                {
                    rows.Add(reader[0].ToString());
                }

                if (rows.Count > 0)
                {
                    root.ro.messages.Add("Executed SQL Query Successfully.");
                    root.ro.results = rows;
                    return true;
                }
                else
                {
                    root.ro.messages.Add("Execute SQL Query Failed.");
                    if (DEBUG) { root.ro.messages.Add("SQL Query: " + root.server.mssql.sql_query); }
                    return false;
                }
            }
            catch (Exception e)
            {
                root.ro.messages.Add("Execute SQL Query Failed.");
                if (DEBUG) { root.ro.messages.Add("SQL Query: " + root.server.mssql.sql_query); }
                if (DEBUG) { root.ro.messages.Add(e.Message); }
                return false;
            }
        }
    }
}
