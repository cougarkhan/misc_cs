﻿namespace sql_queries_2
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Messages = new System.Windows.Forms.ListBox();
            this.Data = new System.Windows.Forms.ListBox();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // Messages
            // 
            this.Messages.FormattingEnabled = true;
            this.Messages.ItemHeight = 16;
            this.Messages.Location = new System.Drawing.Point(12, 12);
            this.Messages.Name = "Messages";
            this.Messages.Size = new System.Drawing.Size(558, 116);
            this.Messages.TabIndex = 0;
            // 
            // Data
            // 
            this.Data.FormattingEnabled = true;
            this.Data.ItemHeight = 16;
            this.Data.Location = new System.Drawing.Point(12, 148);
            this.Data.Name = "Data";
            this.Data.Size = new System.Drawing.Size(558, 404);
            this.Data.TabIndex = 1;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(13, 569);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(557, 23);
            this.button1.TabIndex = 2;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(582, 607);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.Data);
            this.Controls.Add(this.Messages);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListBox Messages;
        private System.Windows.Forms.ListBox Data;
        private System.Windows.Forms.Button button1;
    }
}

