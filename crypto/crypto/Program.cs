﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace crypto
{
    class Program
    {
        static void Main(string[] args)
        {
            string action = "";
            string filePath = "";
            string passwordToEncrypt = "";
            string passwordForSalt = "50zp8TYMowW2P5LK29T0";

            //Console.WriteLine(args.Length);

            for (int i = 0; i < args.Length; i++)
            {
                //Console.WriteLine(args[i]);
                if (args[i].ToLower().Equals("-a")) { action = args[i+1]; }
                if (args[i].ToLower().Equals("-f")) { filePath = args[i + 1]; }
                if (args[i].ToLower().Equals("-p")) { passwordToEncrypt = args[i + 1]; }
            }

            if (action == "" || filePath == "")
            {
                Console.WriteLine("Command line errors");
                Environment.Exit(2);
            }

            //string action = args[0];
            //string filePath = args[1];
            //string passwordToEncrypt = args[2]; 
            //string passwordForSalt = "fjlsdjflhsgkjdfsgkdfsgk";
            Functions f = new Functions();

            if (action.ToLower().Equals("encrypt"))
            {
                Console.WriteLine("Password to Encrypt: {0}.  Password Salt phrase: {1}", passwordToEncrypt, passwordForSalt);
                string encryptedString = f.Encrypt(passwordToEncrypt, passwordForSalt);
                Console.WriteLine("Encrypted Password: {0}", encryptedString);
                File.WriteAllText(filePath, encryptedString);
            }

            if (action.ToLower().Equals("decrypt"))
            {
                string encryptedString = File.ReadAllText(filePath);
                string decryptedString = f.Decrypt(encryptedString, passwordForSalt);
                Console.WriteLine("Decrypted Password: {0}", decryptedString);
            }
        }
    }
}
