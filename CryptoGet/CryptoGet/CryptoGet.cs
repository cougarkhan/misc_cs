﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Management.Automation;
using System.Security;
using System.IO;

namespace CryptoGet
{
    [Cmdlet(VerbsCommon.Get, "fdf")]
    class CryptoGet : Cmdlet
    {
        private string encryptedFile;

        [Parameter(
            Mandatory = true,
            ValueFromPipelineByPropertyName = true,
            ValueFromPipeline = true,
            Position = 0,
            HelpMessage = "Path of encrypted Password File."
        )]
        [Alias("Path")]
        public string FilePath
        {
            get { return encryptedFile; }
            set { encryptedFile = value; }
        }

        protected override void ProcessRecord()
        {
            WriteObject(FilePath);
        }
    }
}
