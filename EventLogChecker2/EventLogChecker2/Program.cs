﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Diagnostics.Eventing.Reader;
using System.Security;
using System.Globalization;
using System.IO;
using System.Management.Automation;

namespace EventLogChecker2
{
    class Program
    {
        static void Main(string[] args)
        {
            string dt = DateTime.Now.ToString("yyyyMMddHHmmss");

            EventLog appLog = new EventLog("Application");
            EventLog secLog = new EventLog("Security");
            EventLog sysLog = new EventLog("System");

            List<EventLog> colLogs = new List<EventLog>();
            colLogs.Add(appLog);
            colLogs.Add(secLog);
            colLogs.Add(sysLog);
            

            EventLogEntryCollection appEntries = appLog.Entries;
            EventLogEntryCollection secEntries = secLog.Entries;
            EventLogEntryCollection sysEntries = sysLog.Entries;
            List<EventLogEntry> errorEntries = new List<EventLogEntry>();
            

            Parallel.ForEach(colLogs, log =>
            {
                EventLogEntryCollection entries = log.Entries;
                Parallel.ForEach(entries.Cast<EventLogEntry>(), entry =>
                {
                    if (entry.EntryType == EventLogEntryType.Error)
                    {
                        errorEntries.Add(entry);
                        //Console.WriteLine("{0}\t{1}\t{2}\t{3}", log.LogDisplayName, entry.EntryType, entry.InstanceId, entry.TimeGenerated);
                    }
                }
                );
            }
            );

            IEnumerable<EventLogEntry> sorted = errorEntries.OrderBy(entry => entry.TimeGenerated);
            //Console.WindowWidth = 192;
            //Console.BufferWidth = 192;

            //PowerShell ps = PowerShell.Create();
            
            //ps.AddScript("Get-WinEvent -FilterHashtable @{LogName=\"Application\"; Level=2; StartTime=((get-date).AddDays(-7))}");
            //Console.WriteLine("{0,-8}{1,-20}{2,-20}{3,-20}{4,-20}{5,-100}", "Id", "TimeCreated", "LogName", "Type", "MachineName", "Message");
            //foreach (PSObject result in ps.Invoke())
            //{
            //    if (Convert.ToInt32((result.Members["Level"].Value)) == 2)
            //    {
            //        Console.WriteLine("{0,-8}{1,-20}{2,-20}{3,-20}{4,-20}{5,-100}", 
            //            result.Members["ID"].Value, 
            //            result.Members["TimeCreated"].Value,
            //            result.Members["LogName"].Value,
            //            result.Members["LevelDisplayName"].Value,
            //            result.Members["MachineName"].Value,
            //            result.Members["Message"].Value);
            //    }
            //}



            Console.WriteLine("\nThe End.");
            Console.ReadKey();
        }
    }
}
