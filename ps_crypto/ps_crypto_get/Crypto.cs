﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Management.Automation;
using System.Security;
using System.IO;

namespace ps_crypto_get
{
    [Cmdlet(VerbsCommon.Get, "PWFile")]
    public class CryptoGet : Cmdlet
    {
        private string encryptedFile;
        private bool DEBUG = false;

        [Parameter(
            Mandatory = true,
            ValueFromPipelineByPropertyName = true,
            ValueFromPipeline = true,
            Position = 0,
            HelpMessage = "Path of encrypted Password File."
        )]
        [Alias("Path")]
        public string FilePath
        {
            get { return encryptedFile; }
            set { encryptedFile = value; }
        }

        protected override void ProcessRecord()
        {
            Functions f = new Functions();

            string passwordForSalt = "50zp8TYMowW2P5LK29T0";

            string encryptedString = File.ReadAllText(FilePath);
            string decryptedString = f.Decrypt(encryptedString, passwordForSalt);

            if (DEBUG)
            {
                Console.WriteLine("Decrypted Password: {0}", decryptedString);
            }

            WriteObject(f.GetSecureString(decryptedString));
        }
    }
}
