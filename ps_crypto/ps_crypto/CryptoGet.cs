﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Management.Automation;
using System.Security;
using System.IO;

namespace ps_crypto
{
    [Cmdlet(VerbsCommon.Get, "PWfile")]
    class CryptoGet : PSCmdlet
    {
        private string encryptedFile;
        private string password;

        [Parameter(
            Mandatory = true,
            ValueFromPipelineByPropertyName = true,
            ValueFromPipeline = true,
            Position = 0,
            HelpMessage = "Path of encrypted Password File."
        )]
        [Alias("FilePath", "Path")]
        public string filePath
        {
            get { return encryptedFile; }
            set { encryptedFile = value; }
        }

        public string passwordToEncrypt
        {
            get { return password; }
            set { password = value; }
        }

        protected override void ProcessRecord()
        {
            Functions f = new Functions();

            string passwordForSalt = "50zp8TYMowW2P5LK29T0";

            string encryptedString = File.ReadAllText(filePath);
            string decryptedString = f.Decrypt(encryptedString, passwordForSalt);
            Console.WriteLine("Decrypted Password: {0}", decryptedString);
        }
    }
}
