﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Management.Automation;
using System.Security;
using System.IO;

namespace ps_crypto
{
    [Cmdlet(VerbsCommon.Set, "PWfile")]
    public class CryptoSet : Cmdlet
    {
        private string encryptedFile;
        private string password;
        private bool DEBUG = false;

        [Parameter(
            Mandatory = true,
            ValueFromPipelineByPropertyName = true,
            ValueFromPipeline = true,
            Position = 0,
            HelpMessage = "Path of encrypted Password File."
        )]
        [Alias("Path")]
        public string FilePath
        {
            get { return encryptedFile; }
            set { encryptedFile = value; }
        }

        public string passwordToEncrypt
        {
            get { return password; }
            set { password = value; }
        }

        protected override void ProcessRecord()
        {
            Functions f = new Functions();

            passwordToEncrypt = f.SetSecureString();

            string passwordForSalt = "50zp8TYMowW2P5LK29T0";

            if (DEBUG)
            {
                Console.WriteLine("Password to Encrypt: {0}.  Password Salt phrase: {1}", passwordToEncrypt, passwordForSalt);
            }
            string encryptedString = f.Encrypt(passwordToEncrypt, passwordForSalt);
            try
            {
                File.WriteAllText(FilePath, encryptedString);
                Console.WriteLine("\nEncrypted Password: {0}", encryptedString);

                WriteObject(FilePath);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

        }
    }
}
