﻿public enum MessageType
{
    Information,
    Warning,
    Error
}