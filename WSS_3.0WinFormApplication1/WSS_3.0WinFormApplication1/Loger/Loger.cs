﻿using System;
using System.IO;

namespace WSS_3._0WinFormApplication1
{
    internal sealed class Loger : IDisposable
    {
        private const String LOG_TIME_FORMAT = "yyyy-MM-dd HH-mm-ss";
        private String logsFolderName;
        private Boolean loggingStarted;
        private String filePath;
        private StreamWriter logWriter;

        public Loger(String logFileName, String logsFolderName = "Logs")
        {
            this.logsFolderName = logsFolderName;
            this.filePath = String.Format("{0}\\{1}[{2}].log", logsFolderName, logFileName, DateTime.Now.ToString(LOG_TIME_FORMAT));
        }

        public void AddDataToLogFormat(MessageType mType, String textTemplate, params Object[] args)
        {
            AddDataToLogInternal(mType, String.Format(textTemplate, args), String.Empty);
        }

        public void AddDataToLog(MessageType mType, String message)
        {
            AddDataToLogInternal(mType, message, String.Empty);
        }

        public void AddDataToLog(MessageType mType, String message, String data)
        {
            AddDataToLogInternal(mType, message, data);
        }

        public void AddDataToLogInternal(MessageType mType, String message, String data)
        {
            SaveDataToDiskInternal(String.Format("{0}{1}{2}: {3}{1}{4}", DateTime.Now, Environment.NewLine, mType.ToString(), message, data));
        }

        private void SaveDataToDiskInternal(String data)
        {
            if (!loggingStarted)
            {
                this.loggingStarted = true;
                if (!Directory.Exists(logsFolderName))
                {
                    Directory.CreateDirectory(logsFolderName);
                }
                this.logWriter = File.AppendText(filePath);
            }
            try
            {
                logWriter.WriteLine(data);
                logWriter.Flush();
            }
            catch { }
        }

        public void Dispose()
        {
            logWriter.Close();
        }
    }
}