﻿using System;

namespace WSS_3._0WinFormApplication1
{
    internal class SharedConstants
    {
        public const String APP_HEADER = "";
        public const String APP_DESCRIPTION = "";
        public const String PROCESSED_LIST_URL = "";
        public const UInt32 SIZE_ITEMS_FOR_PROCESSING = 2000;
        public const String PROCESSING_ITEM_VIEW_FIELDS = "<FieldRef Name='ID'/><FieldRef Name='Title'/>";
        public const String PROCESSING_ITEMS_QUERY = "";
    }
}