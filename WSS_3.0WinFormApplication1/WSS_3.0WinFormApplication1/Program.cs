﻿using System;
using System.Windows.Forms;

namespace WSS_3._0WinFormApplication1
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            try
            {
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.Run(new WSSForm(args));
            }
            catch { }
        }
    }
}
