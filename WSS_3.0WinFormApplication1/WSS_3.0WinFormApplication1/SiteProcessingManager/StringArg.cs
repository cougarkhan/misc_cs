﻿
using System;

namespace WSS_3._0WinFormApplication1
{
    internal class StringArg : EventArgs
    {
        public StringArg(String stringValue)
        {
            this.StringArgValue = stringValue;
        }

        public String StringArgValue { get; set; }
    }
}
