﻿using System;

namespace WSS_3._0WinFormApplication1
{
    internal class NumberArg : EventArgs
    {
        public Int32 NumberArgValue { get; set; }
        public NumberArg(Int32 numberValue)
        {
            this.NumberArgValue = numberValue;
        }
    }
}