﻿using Microsoft.SharePoint;
using System;

namespace WSS_3._0WinFormApplication1
{
    internal partial class SiteProcessingManager
    {
        public event EventHandler OnExecutionEnded;
        public event EventHandler OnOperationProcessed;
        public event EventHandler<StringArg> OnProcessedListChanged;
        public event EventHandler<NumberArg> OnAmountOfWorkDefined;
        public event EventHandler<StringArg> OnActionChanged;

        private void DefineAmountOfWork(SPList list)
        {
            OnAmountOfWorkDefined(this, new NumberArg(list.ItemCount));
        }

        private void DefineProcessingListName(String stringIdentifier)
        {
            OnProcessedListChanged(this, new StringArg(stringIdentifier));
        }

        private void DefineAction(String actionName)
        {
            OnActionChanged(this, new StringArg(actionName));
        }
    }
}
