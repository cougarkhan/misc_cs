﻿using Microsoft.SharePoint;
using RADS.Core;
using System;

namespace WSS_3._0WinFormApplication1
{
    internal partial class SiteProcessingManager
    {
        private SPWeb ContextWeb { get; set; }
        private String[] Args { get; set; }
        private BatchBuilder BatchBuilder { get; set; }
        private Loger loger;

        public SiteProcessingManager(String[] args)
        {
            this.Args = args;
            this.loger = new Loger(SharedConstants.APP_HEADER);
        }

        public void StartWork()
        {
            try
            {
                String siteUrl = null;
                if (Args.Length > 0)
                {
                    siteUrl = Args[0];
                }
                else
                {
                    siteUrl = "http://localhost";
                }

                SPSecurity.RunWithElevatedPrivileges(delegate()
                {
                    using (SPSite site = new SPSite(siteUrl))
                    {
                        using (SPWeb web = site.OpenWeb())
                        {
                            try
                            {
                                SPEvents events = new SPEvents();
                                events.DisableEvents();
                                ContextWeb = web;
                                ProcessSite();
                                events.EnableEvents();
                            }
                            catch (Exception e)
                            {
                                loger.AddDataToLog(MessageType.Error, e.Message, e.StackTrace);
                            }
                        }
                    }
                });
            }
            catch (Exception e)
            {
                loger.AddDataToLog(MessageType.Error, e.Message, e.StackTrace);
            }
            finally
            {
                loger.AddDataToLog(MessageType.Information, "Processing done");
                loger.Dispose();
                OnExecutionEnded(this, new EventArgs());
            }
        }

        private void ProcessSite()
        {
            DefineAction("Getting the list");
            SPList spList = ContextWeb.GetList(SharedConstants.PROCESSED_LIST_URL);
            DefineProcessingListName(spList.Title);

            DefineAction("Estimation of the work");
            DefineAmountOfWork(spList);

            loger.AddDataToLogFormat(MessageType.Information, "Start processing of '{0} list'", spList.Title);
            ProcessItems(spList);
        }

        private void ProcessItems(SPList spList)
        {
            String pagingInfo = String.Empty;

            SPListItemCollection items = null;

            do
            {
                DefineAction("Collecting data for processing");
                if (items != null)
                {
                    pagingInfo = items.ListItemCollectionPosition.PagingInfo;
                }
                items = GetPagedItems(spList, pagingInfo, SharedConstants.SIZE_ITEMS_FOR_PROCESSING);

                Int32 itemsCount = items.Count;
                if (itemsCount > 0)
                {
                    BatchBuilder = new BatchBuilder();
                    for (Int32 index = 0; index < itemsCount; index++)
                    {
                        try
                        {
                            UpdateItem(items[index], BatchBuilder);
                        }
                        catch (Exception e)
                        {
                            loger.AddDataToLog(MessageType.Error, e.Message, e.StackTrace);
                        }
                        finally
                        {
                            OnOperationProcessed(this, new EventArgs());
                        }
                    }
                    DefineAction("Processing of batch data");
                    String batchWorkResults = ContextWeb.ProcessBatchData(BatchBuilder.FinalizeBatch());

                    DefineAction("Saving of work results");
                    loger.AddDataToLog(MessageType.Information, "Batch Work Results", batchWorkResults);
                }
            }
            while (items.ListItemCollectionPosition != null);
        }

        private SPListItemCollection GetPagedItems(SPList list, String pagingInfo, UInt32 rowLimit)
        {
            SPQuery query = new SPQuery();
            query.RowLimit = rowLimit;
            query.ListItemCollectionPosition = new SPListItemCollectionPosition(pagingInfo);
            query.ViewAttributes = "Scope='Recursive'";
            query.ViewFields = SharedConstants.PROCESSING_ITEM_VIEW_FIELDS;
            query.Query = SharedConstants.PROCESSING_ITEMS_QUERY;

            return list.GetItems(query);
        }

        private void UpdateItem(SPListItem listItem, BatchBuilder batchBuilder)
        {
            loger.AddDataToLogFormat(MessageType.Information, "Begin processing of item with ID: '{0}' in '{1}' list", listItem.ID, listItem.ParentList.Title);

            if (true) //some condition, which determine - should we update item or not
            {
                //add data to the batch
                String fieldName = null;
                String fieldValue = null;
                batchBuilder.AppendFormatBatchString(@"<Method ID='{0}'>
                                                              <SetList>{1}</SetList>
                                                              <SetVar Name='Cmd'>Save</SetVar>
                                                              <SetVar Name='ID'>{0}</SetVar>
                                                              <SetVar Name='urn:schemas-microsoft-com:office:office#{2}'>{3}</SetVar>
                                                      </Method>",
                                                              listItem.ID, listItem.ParentList.ID, fieldName, fieldValue);

                loger.AddDataToLogFormat(MessageType.Information, "Item with ID: '{0}' was added for batch update", listItem.ID);
            }
            else
            {
                loger.AddDataToLogFormat(MessageType.Information, "Processing of item with ID: '{0}' is not required", listItem.ID);
            }
        }
    }
}