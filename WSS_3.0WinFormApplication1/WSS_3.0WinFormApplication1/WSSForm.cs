﻿using System;
using System.Threading;
using System.Windows.Forms;

namespace WSS_3._0WinFormApplication1
{
    public partial class WSSForm : Form
    {
        private const String ABOUT_TEXT_HEADER = "About";
        private String[] Args { get; set; }

        internal delegate void WriteDelegate(String message);
        private WriteDelegate _setProcessingListTextWorker;
        internal WriteDelegate SetProcessingListTextWorker
        {
            get
            {
                if (_setProcessingListTextWorker == null)
                {
                    _setProcessingListTextWorker = new WriteDelegate(SetProcessingListTextInternal);
                }
                return _setProcessingListTextWorker;
            }
        }
        internal delegate void SetProgressDelegate();
        private SetProgressDelegate _setInstallationProgressWorker;
        internal SetProgressDelegate SetInstallationProgressWorker
        {
            get
            {
                if (_setInstallationProgressWorker == null)
                {
                    _setInstallationProgressWorker = new SetProgressDelegate(IncreaseProgressBarStepInternal);
                }
                return _setInstallationProgressWorker;
            }
        }
        internal WriteDelegate _setProcessingActionWorker;
        internal WriteDelegate SetProcessingActionWorker
        {
            get
            {
                if (_setProcessingActionWorker == null)
                {
                    _setProcessingActionWorker = new WriteDelegate(SetProcessingActionInternal);
                }
                return _setProcessingActionWorker;
            }
        }
        private Thread processingSiteThread;

        public WSSForm(String[] args)
        {
            InitializeComponent();
            this.Text = SharedConstants.APP_HEADER;
            this.Args = args;
        }

        #region Manage UI methods
        internal void SetProcessingListText(String processingList)
        {
            if (this.InvokeRequired)
            {
                BeginInvoke(SetProcessingListTextWorker, processingList);
            }
            else
            {
                SetProcessingListTextInternal(processingList);
            }
        }

        internal void SetProgressBarMaximum(Int32 progressMaximum)
        {
            if (this.InvokeRequired)
            {
                IAsyncResult result = BeginInvoke(new Action(() =>
                {
                    this.progressBar.Maximum = progressMaximum;
                }));
            }
            else
            {
                this.progressBar.Maximum = progressMaximum;
            }
        }

        internal void SetProcessingAction(String actionName)
        {
            if (InvokeRequired)
            {
                BeginInvoke(SetProcessingActionWorker, actionName);
            }
            else
            {
                SetProcessingActionInternal(actionName);
            }
        }

        internal void IncreaseProgressBarStep()
        {
            if (this.InvokeRequired)
            {
                BeginInvoke(SetInstallationProgressWorker);
            }
            else
            {
                IncreaseProgressBarStepInternal();
            }
        }

        internal void FormClose()
        {
            if (this.InvokeRequired)
            {
                BeginInvoke(new Action(() =>
                {
                    this.Close();
                }));
            }
            else
            {
                this.Close();
            }
        }

        private void SetProcessingListTextInternal(String listName)
        {
            this.lblProcessingListValue.Text = listName;
        }

        private void IncreaseProgressBarStepInternal()
        {
            if (this.progressBar.Value < this.progressBar.Maximum)
            {
                ++this.progressBar.Value;
                Decimal totalProgress = ((Decimal)this.progressBar.Value / this.progressBar.Maximum) * 100;
                this.lblProgress.Text = String.Format("{0}%", totalProgress.ToString("#.##"));
            }
        }

        private void SetProcessingActionInternal(String actionName)
        {
            this.lblProcessingActionValue.Text = actionName;
        }

        #endregion

        #region Events

        private void WSS_Form_Load(object sender, EventArgs e)
        {
            processingSiteThread = new Thread(new ThreadStart(delegate()
            {
                SiteProcessingManager siteProcManager = new SiteProcessingManager(Args);
                siteProcManager.OnExecutionEnded += SiteProcManagerOnExecutionEnded;
                siteProcManager.OnOperationProcessed += SiteProcManagerOnOperationProcessed;
                siteProcManager.OnProcessedListChanged += SiteProcManagerOnProcessedListChanged;
                siteProcManager.OnAmountOfWorkDefined += SiteProcManagerOnAmountOfWorkDefined;
                siteProcManager.OnActionChanged += SiteProcManagerOnAdditionalActionChanged;
                siteProcManager.StartWork();

            }));
            processingSiteThread.IsBackground = true;
            processingSiteThread.Start();
        }

        void SiteProcManagerOnAdditionalActionChanged(object sender, StringArg e)
        {
            SetProcessingAction(e.StringArgValue);
        }

        private void SiteProcManagerOnAmountOfWorkDefined(object sender, NumberArg e)
        {
            SetProgressBarMaximum(e.NumberArgValue);
        }

        private void SiteProcManagerOnProcessedListChanged(object sender, StringArg e)
        {
            SetProcessingListText(e.StringArgValue);
        }

        private void SiteProcManagerOnOperationProcessed(object sender, EventArgs e)
        {
            IncreaseProgressBarStep();
        }

        private void SiteProcManagerOnExecutionEnded(object sender, EventArgs e)
        {
            FormClose();
        }

        private void PictureBoxClick(object sender, EventArgs e)
        {
            MessageBox.Show(SharedConstants.APP_DESCRIPTION, ABOUT_TEXT_HEADER, MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        #endregion
    }
}