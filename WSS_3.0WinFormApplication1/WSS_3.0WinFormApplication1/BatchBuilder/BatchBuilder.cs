﻿using System;
using System.Text;

namespace WSS_3._0WinFormApplication1
{
    internal class BatchBuilder
    {
        private const String BATCH_HEAD_TEMPLATE = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><ows:Batch OnError=\"{0}\">";
        private const String BATCH_END = "</ows:Batch>";

        private StringBuilder batchStrBuilder;
        public BatchBuilder() : this(BatchActionOnError.Continue) { }

        public BatchBuilder(BatchActionOnError actionOnError)
        {
            string bathHead = String.Format(BATCH_HEAD_TEMPLATE, actionOnError);
            this.batchStrBuilder = new StringBuilder();
            this.batchStrBuilder.Append(bathHead);
        }

        public void AppendBatchString(String batchPart)
        {
            this.batchStrBuilder.Append(batchPart);
        }

        public void AppendFormatBatchString(String templateText, params Object[] args)
        {
            this.batchStrBuilder.AppendFormat(templateText, args);
        }

        public string FinalizeBatch()
        {
            this.batchStrBuilder.Append(BATCH_END);
            return this.batchStrBuilder.ToString();
        }
    }
}
