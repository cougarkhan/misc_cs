﻿namespace WSS_3._0WinFormApplication1
{
    partial class WSSForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(WSSForm));
            this.progressBar = new System.Windows.Forms.ProgressBar();
            this.lblProcessingListText = new System.Windows.Forms.Label();
            this.lblProcessingListValue = new System.Windows.Forms.Label();
            this.pictureBox = new System.Windows.Forms.PictureBox();
            this.lblProcessingActionText = new System.Windows.Forms.Label();
            this.lblProcessingActionValue = new System.Windows.Forms.Label();
            this.lblProgress = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // progressBar
            // 
            this.progressBar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.progressBar.Location = new System.Drawing.Point(12, 85);
            this.progressBar.Name = "progressBar";
            this.progressBar.Size = new System.Drawing.Size(260, 15);
            this.progressBar.TabIndex = 0;
            // 
            // lblProcessingListText
            // 
            this.lblProcessingListText.AutoSize = true;
            this.lblProcessingListText.Location = new System.Drawing.Point(9, 33);
            this.lblProcessingListText.Name = "lblProcessingListText";
            this.lblProcessingListText.Size = new System.Drawing.Size(81, 13);
            this.lblProcessingListText.TabIndex = 1;
            this.lblProcessingListText.Text = "Processing List:";
            // 
            // lblProcessingListValue
            // 
            this.lblProcessingListValue.AutoSize = true;
            this.lblProcessingListValue.Location = new System.Drawing.Point(96, 33);
            this.lblProcessingListValue.Name = "lblProcessingListValue";
            this.lblProcessingListValue.Size = new System.Drawing.Size(10, 13);
            this.lblProcessingListValue.TabIndex = 3;
            this.lblProcessingListValue.Text = "-";
            // 
            // pictureBox
            // 
            this.pictureBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox.Image")));
            this.pictureBox.InitialImage = null;
            this.pictureBox.Location = new System.Drawing.Point(256, 6);
            this.pictureBox.Name = "pictureBox";
            this.pictureBox.Size = new System.Drawing.Size(16, 16);
            this.pictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox.TabIndex = 5;
            this.pictureBox.TabStop = false;
            this.pictureBox.Click += new System.EventHandler(this.PictureBoxClick);
            // 
            // lblProcessingActionText
            // 
            this.lblProcessingActionText.AutoSize = true;
            this.lblProcessingActionText.Location = new System.Drawing.Point(9, 9);
            this.lblProcessingActionText.Name = "lblProcessingActionText";
            this.lblProcessingActionText.Size = new System.Drawing.Size(77, 13);
            this.lblProcessingActionText.TabIndex = 6;
            this.lblProcessingActionText.Text = "Current Action:";
            // 
            // lblProcessingActionValue
            // 
            this.lblProcessingActionValue.AutoSize = true;
            this.lblProcessingActionValue.Location = new System.Drawing.Point(96, 9);
            this.lblProcessingActionValue.Name = "lblProcessingActionValue";
            this.lblProcessingActionValue.Size = new System.Drawing.Size(10, 13);
            this.lblProcessingActionValue.TabIndex = 7;
            this.lblProcessingActionValue.Text = "-";
            // 
            // lblProgress
            // 
            this.lblProgress.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblProgress.AutoSize = true;
            this.lblProgress.Location = new System.Drawing.Point(13, 66);
            this.lblProgress.Name = "lblProgress";
            this.lblProgress.Size = new System.Drawing.Size(13, 13);
            this.lblProgress.TabIndex = 8;
            this.lblProgress.Text = "0";
            // 
            // WSSForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 112);
            this.Controls.Add(this.lblProgress);
            this.Controls.Add(this.lblProcessingActionValue);
            this.Controls.Add(this.lblProcessingActionText);
            this.Controls.Add(this.pictureBox);
            this.Controls.Add(this.lblProcessingListValue);
            this.Controls.Add(this.lblProcessingListText);
            this.Controls.Add(this.progressBar);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(300, 150);
            this.Name = "WSSForm";
            this.Tag = "";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.WSS_Form_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ProgressBar progressBar;
        private System.Windows.Forms.Label lblProcessingListText;
        private System.Windows.Forms.Label lblProcessingListValue;
        private System.Windows.Forms.PictureBox pictureBox;
        private System.Windows.Forms.Label lblProcessingActionText;
        private System.Windows.Forms.Label lblProcessingActionValue;
        private System.Windows.Forms.Label lblProgress;
    }
}

