﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.Runtime.InteropServices;
using Outlook = Microsoft.Office.Interop.Outlook;

namespace outlook_archive
{
    class Program
    {
        static void Main(string[] args)
        {
            Outlook.Application app = new Outlook.Application();
            Outlook.NameSpace ns= app.GetNamespace("MAPI");

            ns.Logon("", "", Missing.Value, Missing.Value);
            Outlook.MAPIFolder inbox = ns.GetDefaultFolder(Outlook.OlDefaultFolders.olFolderInbox);

            object selectedItem = app.ActiveExplorer().Selection[1];

            if (selectedItem is Outlook.MailItem)
            {
                Outlook.MailItem mailItem = selectedItem as Outlook.MailItem;

            Console.ReadKey();
        }
    }
}
