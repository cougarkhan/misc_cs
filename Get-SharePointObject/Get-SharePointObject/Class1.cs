﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Management.Automation;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Net;
using System.IO;

namespace Get_SharePointObject
{
    [Cmdlet(VerbsCommon.Get, "SharePointObject")]
    public class SharePointObject : PSCmdlet
    {
        private string spoURI;
        [Parameter(
            Mandatory=true,
            ValueFromPipelineByPropertyName=true,
            ValueFromPipeline=true,
            Position=0,
            HelpMessage="Enter URI to connect to."
            )]
        [Alias("URL")]
        public string Uri
        {
            get { return spoURI; }
            set { spoURI = value; }
        }

        private string[] spoColumns;
        [Parameter(
            Mandatory = false,
            ValueFromPipelineByPropertyName = false,
            ValueFromPipeline = false,
            Position = 1,
            HelpMessage = "Enter Columns to Retrieve."
            )]
        [Alias("Headings")]
        public string[] Columns
        {
            get { return spoColumns; }
            set { spoColumns = value; }
        }
        
        
        protected override void BeginProcessing()
        {
            base.BeginProcessing();
        }

        protected override void ProcessRecord()
        {
            //if (Columns == null)
            //{
            //    Columns = new string[] {"ID",
            //                            "Title",
            //                            "OS",
            //                            "IP_ADDRESS",
            //                            "DNS_NAME",
            //                            "Interface",
            //                            "ENVIRONMENT",
            //                            "SPEEDCHART",
            //                            "INFRASTRUCTURE",
            //                            "LOCATION",
            //                            "Dept",
            //                            "Remarks",
            //                            "Processor_x0020_Type",
            //                            "Modified",
            //                            "Created"};
            //}

            if (Columns == null)
            {
                Columns = new string[] {"*"};
            }


            HttpWebRequest endpointRequest = (HttpWebRequest)HttpWebRequest.Create(spoURI);
            endpointRequest.Method = "GET";
            endpointRequest.Accept = "application/json;odata=verbose";
            endpointRequest.Credentials = CredentialCache.DefaultCredentials;
            HttpWebResponse endpointResponse = (HttpWebResponse)endpointRequest.GetResponse();
            try
            {
                WebResponse webResponse = endpointRequest.GetResponse();
                Stream webStream = webResponse.GetResponseStream();
                StreamReader responseReader = new StreamReader(webStream);
                string response = responseReader.ReadToEnd();
                JObject jobj = JObject.Parse(response);
                JArray jarr = (JArray)jobj["d"]["results"];

                foreach (JObject j in jarr)
                {
                    string title = j["Title"].ToString();
                    WriteDebug("Creating PSObject " + title);
                    PSObject ps = new PSObject();
                    
                    if (spoColumns.Length == 1 && spoColumns[0] == "*")
                    {
                        foreach (var column in j)
                        {
                            ps.Properties.Add(new PSNoteProperty(column.Key, column.Value.ToString()));
                        }
                    }
                    else
                    {
                        foreach (string column in spoColumns)
                        {
                            WriteDebug("Adding Property " + column);
                            ps.Properties.Add(new PSNoteProperty(column, j[column].ToString()));
                        }
                    }

                    WriteDebug("Writing Object " + title);
                    WriteVerbose("Writing Object " + title);
                    WriteObject(ps);
                }

                responseReader.Close();
                //Console.ReadLine();


            }
            catch (Exception e)
            {
                Console.Out.WriteLine(e);
            }
        }

        protected override void EndProcessing()
        {
            base.EndProcessing();
        }
    }
}
