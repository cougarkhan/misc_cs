﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace SickBeard
{
    public partial class Service1 : ServiceBase
    {
        private static EventLog ev = new EventLog();
        private static Process sickbeard = new Process();
        private static int sb_id;

        public Service1()
        {
            InitializeComponent();
            if (!EventLog.SourceExists("SickBeard"))
            {
                EventLog.CreateEventSource("SickBeard", "Application");
            }
            ev.Source = "SickBeard";
            ev.Log = "Application";
            //sickbeard.StartInfo.FileName = @"C:\Temp\SickBeard\sickbeard.exe";
            sickbeard.StartInfo.FileName = @"C:\Program Files (x86)\SickBeard\sickbeard.exe";
        }

        protected override void OnStart(string[] args)
        {
            sickbeard.Start();
            sb_id = sickbeard.Id;
            ev.WriteEntry("Starting SickBeard Service...\nProcess ID " + sickbeard.Id);
        }

        protected override void OnStop()
        {
            ev.WriteEntry("Stopping SickBeard Service...");
            (Process.GetProcessById(sb_id)).Kill();
        }
    }
}
