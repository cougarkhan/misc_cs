﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Oracle.ManagedDataAccess.Client;
using System.Data.SqlClient;
using System.Data;
//using System.Data.Odbc;

namespace sqlcheck
{
    class Program
    {
        static void Main(string[] args)
        {
            const string ORACLE = "oracle";
            const string MSSQL = "mssql";
            string[] code = { "INFO", "WARNING", "ERROR" };
            Settings config = new Settings(@".\etc\settings.json");
            Oracle oracle = config.server.oracle;
            Mssql mssql = config.server.mssql;
            bool result = new Boolean();

            ProgramFunctions pf = new ProgramFunctions(config.server.debug.flag);
            
            switch (args[0])
            {
                case ORACLE:
                    OracleConnection oracle_conn = pf.GetOracleConnection(oracle);
                    if (oracle_conn == null)
                    {
                        Console.WriteLine("ERROR: Oracle Connection Object could not be created.");
                        Environment.Exit(2);
                    }
                    OracleCommand oracle_cmd = pf.GetOracleCommand(oracle_conn);
                    if (oracle_cmd == null)
                    {
                        Console.WriteLine("ERROR: Oracle Command Object could not be created.");
                        Environment.Exit(2);
                    }
                    oracle_conn = pf.OpenOracleConnection(oracle_conn);
                    if (oracle_conn == null)
                    {
                        Console.WriteLine("ERROR: Oracle Connection could not be opened.");
                        Environment.Exit(2);
                    }
                    if (oracle_conn != null && oracle_conn.State == ConnectionState.Open)
                    {
                        result = pf.ExecuteOracleQuery(oracle_cmd, oracle);
                        oracle_conn = pf.CloseOracleConnection(oracle_conn);
                        if (result)
                        {
                            Console.WriteLine("OK: Oracle SQL Query Succeeded.");
                            Environment.Exit(0);
                        }
                        else 
                        {
                            Console.WriteLine("ERROR: Oracle SQL Query Failed.");
                            Environment.Exit(2);
                        }
                    }
                    break;
                case MSSQL:
                    SqlConnection mssql_conn = pf.GetMSSqlConnection(mssql);
                    if (mssql_conn == null)
                    {
                        Console.WriteLine("ERROR: Oracle Connection Object could not be created.");
                        Environment.Exit(2);
                    }
                    SqlCommand mssql_cmd = pf.GetMSSqlCommand(mssql_conn);
                    if (mssql_cmd == null)
                    {
                        Console.WriteLine("ERROR: Oracle Command Object could not be created.");
                        Environment.Exit(2);
                    }
                    mssql_conn = pf.OpenMSSqlConnection(mssql_conn);
                    if (mssql_conn == null)
                    {
                        Console.WriteLine("ERROR: Oracle Connection could not be opened.");
                        Environment.Exit(2);
                    }
                    if (mssql_conn != null && mssql_conn.State == ConnectionState.Open)
                    {
                        result = pf.ExecuteMSSqlQuery(mssql_cmd, mssql);
                        mssql_conn = pf.CloseMSSqlConnection(mssql_conn);
                        if (result)
                        {
                            Console.WriteLine("OK: MSSQL SQL Query Succeeded.");
                            Environment.Exit(0);
                        }
                        else 
                        {
                            Console.WriteLine("ERROR: MSSQL SQL Query Failed.");
                            Environment.Exit(2);
                        }
                    }
                    break;
                default:
                    Environment.Exit(03);
                    break;
            }

            if (config.server.debug.flag)
            {
                Console.ReadLine();
            }
        }
    }
}
