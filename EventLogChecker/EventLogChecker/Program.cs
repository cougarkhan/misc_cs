﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Diagnostics.Eventing.Reader;
using System.Security;
using System.Globalization;
using System.IO;

namespace EventLogChecker
{
    class Program
    {
        static void Main(string[] args)
        {

            string[] servers = System.IO.File.ReadAllLines(@"C:\temp\servers.txt");
            string dt = DateTime.Now.ToString("yyyyMMddHHmmss");

            Console.WriteLine("{0}", dt);

            Parallel.ForEach(servers, server =>
                {
                    try
                    {
                        EventLogSession es = new EventLogSession(server);
                        es.ExportLogAndMessages("Application", PathType.LogName, "*", @"\\BIS-SQLDSX-DEV\c$\DRS\Logexporttest-" + dt + ".evtx", false, CultureInfo.CurrentCulture);
                        Console.WriteLine("Exported and Archived the Security log to the {0}\\C\\Temp\\Logexporttest-" + dt + ".evtx file.", server);
                    }
                    //catch (UnauthorizedAccessException e)
                    //{
                    //    //  requires elevation of privilege
                    //    Console.WriteLine("You do not have the correct permissions. " +
                    //        "Try running with administrator privileges. " + e.ToString());
                    //}
                    //catch (EventLogNotFoundException e)
                    //{
                    //    //  The target log may not exist
                    //    Console.WriteLine(e.ToString());
                    //}
                    //catch (EventLogException e)
                    //{
                    //    // The target file may already exist
                    //    Console.WriteLine(e.ToString());
                    //}
                    catch (Exception e)
                    {
                        Console.WriteLine("Errors on {0}. {1}", server, e.InnerException);
                    }
                }
            );


              
                
            
            //EventLog appLog = new EventLog("Application");
            //EventLogEntryCollection entries = appLog.Entries;
            
            //foreach (EventLogEntry entry in entries)
            //{
            //    if (entry.EntryType == EventLogEntryType.Error)
            //    {
            //        Console.WriteLine("{0}\t{1}\t{2}", entry.EntryType, entry.InstanceId, entry.TimeGenerated);
            //    }
            //}

            Console.WriteLine("\r\rThe End.");
            Console.ReadKey();
        }
    }
}
