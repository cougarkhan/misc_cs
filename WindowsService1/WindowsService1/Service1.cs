﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace WindowsService1
{
    public partial class Service1 : ServiceBase
    {
        static EventLog ev1 = new EventLog();
        static Process sickbeard = new Process();
        static int sickbeardID;

        public Service1()
        {
            InitializeComponent();

            if (!EventLog.SourceExists("TestService"))
            {
                EventLog.CreateEventSource("TestService", "Application");
            }
            ev1.Source = "TestService";
            ev1.Log = "Application";
            sickbeard.StartInfo.FileName = @"C:\Temp\Sickbeard\sickbeard.exe";
        }

        protected override void OnStart(string[] args)
        {
            ev1.WriteEntry("Test Service is starting...");
            sickbeard.Start();
            sickbeardID = sickbeard.Id;
        }

        protected override void OnStop()
        {
            ev1.WriteEntry("Test Service is stopping...");
            if (!sickbeard.HasExited)
            {
                sickbeard.Kill();
            }
        }

        protected override void OnContinue()
        {
            ev1.WriteEntry("Test Service is continuing...");
        }

        static void CheckSickbeard()
        {
            Process p = Process.GetProcessById(sickbeardID);
            if (p == null)
            {
                sickbeard.Start();
                sickbeardID = sickbeard.Id;
                ev1.WriteEntry("Restarted Sickbeard");
            }
        }

        void Main()
        {
            System.Threading.Thread.Sleep(5000);
            CheckSickbeard();
        }

        
    }
}
