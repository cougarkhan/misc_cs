﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.DirectoryServices;

namespace EventLogChecker3
{
    class Program
    {
        static void Main(string[] args)
        {
            Functions f = new Functions();

            List<string> serverNames = f.GetNames();

            Parallel.ForEach (serverNames, s =>
            {
                try
                {
                    f.GetEventLog(s, "Application");
                    //Console.WriteLine("Exported Application log for " + s);
                }
                catch (Exception e)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine(s);
                    Console.ForegroundColor = ConsoleColor.White;
                }
            }
            );



            Console.WriteLine("\n\nThe End.");
            Console.ReadKey();
        }
    }
}
