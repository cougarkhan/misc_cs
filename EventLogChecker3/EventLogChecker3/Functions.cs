﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.DirectoryServices;
using System.Diagnostics;
using System.Diagnostics.Eventing.Reader;

namespace EventLogChecker3
{
    class Functions
    {
        public List<string> GetNames()
        {
            List<string> serverNames = new List<string>();

            DirectoryEntry ldapConn = new DirectoryEntry("bussops.ubc.ca");
            ldapConn.Path = "LDAP://DC=bussops,DC=ubc,DC=ca";
            ldapConn.AuthenticationType = AuthenticationTypes.Secure;

            DirectorySearcher search = new DirectorySearcher(ldapConn);
            search.Filter = ("operatingsystem=*Server*");
            search.PropertiesToLoad.Add("DNSHostName");

            SearchResultCollection results = search.FindAll();

            if (results.Count > 0)
            {
                foreach (SearchResult result in results)
                {
                    serverNames.Add(result.Properties["DNSHostName"][0].ToString());
                }
            }

            return serverNames;
        }

        public void GetEventLog(string serverName, string evName)
        {
            //EventLog ev = new EventLog(evName,serverName);
            EventLogSession evs = new EventLogSession(serverName);
            string date = (DateTime.Now).ToString("yyyyMMddHHmmss");
            evs.ExportLogAndMessages(evName, PathType.LogName, "*", @"C:\DRS\" + evName + "-" + date + ".evtx");
        }
    }
}
