﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Management;
using System.Management.Automation;

namespace get_serverinfo
{
    [Cmdlet(VerbsCommon.Get, "SysInfo")]
    public class serverinfo : Cmdlet
    {
        [Parameter(Mandatory = true, ValueFromPipeline=true, ValueFromPipelineByPropertyName=true,Position=0)]
        public string server
        {
            get { return server_name; }
            set { server_name = value; }
        }
        private string server_name;
        server s = new server();

        protected override void ProcessRecord()
        {
            WqlObjectQuery diskSelectQuery, networkSelectQuery;
            ManagementObjectSearcher diskSearcher, networkSearcher;

            //Disks
            diskSelectQuery = new WqlObjectQuery("SELECT * FROM Win32_LogicalDisk WHERE DriveType=3");
            diskSearcher = new ManagementObjectSearcher(diskSelectQuery);
            s.disks = diskSearcher.Get();

            //Network
            networkSelectQuery = new WqlObjectQuery("SELECT DNSHostName,IPAddress,IPSubnet,DefaultIPGateway,MACAddress,DNSDomain,DNSDomainSuffixSearchOrder FROM Win32_NetworkAdapterConfiguration WHERE IPEnabled = True");
            networkSearcher = new ManagementObjectSearcher(networkSelectQuery);
            
            s.disks = diskSearcher.Get();
            s.network = networkSearcher.Get();

            WriteObject(s);
        }

    }
}
