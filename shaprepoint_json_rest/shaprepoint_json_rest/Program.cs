﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Net;
using System.IO;

namespace shaprepoint_json_rest
{
    class Program
    {
        static void Main(string[] args)
        {
            HttpWebRequest endpointRequest = (HttpWebRequest)HttpWebRequest.Create("https://bis.share.ubc.ca/AdminOps/_api/web/lists/GetByTitle('AllServers')/items");

            endpointRequest.Method = "GET";
            endpointRequest.Accept = "application/json;odata=verbose";
            //NetworkCredential cred = new System.Net.NetworkCredential();
            endpointRequest.Credentials = CredentialCache.DefaultCredentials;
            HttpWebResponse endpointResponse = (HttpWebResponse)endpointRequest.GetResponse();
            try
            {
                WebResponse webResponse = endpointRequest.GetResponse();
                Stream webStream = webResponse.GetResponseStream();
                StreamReader responseReader = new StreamReader(webStream);
                string response = responseReader.ReadToEnd();
                JObject jobj = JObject.Parse(response);
                JArray jarr = (JArray)jobj["d"]["results"];
                foreach (JObject j in jarr)
                {
                    Console.WriteLine(j["Title"] + " " + j["OS"]);
                }

                responseReader.Close();
                Console.ReadLine();


            }
            catch (Exception e)
            {
                Console.Out.WriteLine(e.Message); Console.ReadLine();
            } 
        }
    }
}
